# Copyright 2009-2014 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Swedish implementation of Kerberos 5"
DESCRIPTION="
Heimdal is an implementation of Kerberos 5 (and some more stuff) largely written in Sweden.
"
HOMEPAGE="http://www.h5l.org"
DOWNLOADS="${HOMEPAGE}/dist/src/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    X
    afs [[ description = [ Add support for Andrew's File System ] ]]
    berkdb
    ldap
    otp [[ description = [ Add support for One-Time Password ] ]]
    threads
"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
        virtual/awk
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-db/sqlite:3
        sys-fs/e2fsprogs
        sys-libs/libcap-ng[>=0.4.0]
        sys-libs/ncurses
        sys-libs/readline:=
        X? (
            x11-libs/libX11
            x11-libs/libXt
        )
        afs? ( net-fs/openafs )
        berkdb? ( sys-libs/db:4.6 )
        !berkdb? ( sys-libs/gdbm )
        ldap? ( net-directory/openldap )
        !app-crypt/krb5 [[
                description = [ (MIT) krb5 and Heimdal collide. Choose one. ]
                url = [ http://dev.exherbo.org/~philantrop/heimdal-krb5-collisions.txt ]
                resolution = manual
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/1846c7a35d1091d3b6140c56b.patch
    "${FILES}"/${PN}-1.5.3-missing-symbols.patch
)

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

DEFAULT_SRC_INSTALL_PARAMS=( 'INSTALL_CATPAGES=no' )

src_configure() {
    # for (lib)hcrypto, provides a openssl (libcrypto) compatible interface
    # LIBS=-pthread: http://www.stacken.kth.se/lists/heimdal-discuss/2004-06/msg00081.html
    local myconf=(
        --enable-kcm
        --enable-kx509
        --enable-pk-init
        --disable-osfc2
        --disable-static
        --with-capng
        --with-ipv6
        --with-readline=/usr/$(exhost --target)
        --with-sqlite3=/usr/$(exhost --target)
        --without-openssl
        $(option_with X x)
        $(option_with berkdb berkeley-db)
        $(option_enable afs afs-support)
        $(option_enable !berkdb ndbm-db)
        $(option_enable otp)
        $(option_enable threads pthread-support)
        $(option threads && echo 'LIBS=-pthread')
    )

    if option ldap; then
        myconf+=( --with-openldap=/usr/$(exhost --target)
                  --enable-hdb-openldap-module )
    else
        myconf+=( --without-openldap
                  --disable-hdb-openldap-module )
    fi

    econf ${myconf[@]}
}

src_install() {
    local host=$(exhost --target)
    default

    # Do some renames to avoid several collisions
    for i in rcp rsh telnet ftp su login pagsh kf; do
        edo mv "${IMAGE}"/usr/share/man/man1/{,k}${i}.1
        edo mv "${IMAGE}"/usr/${host}/bin/{,k}${i}
    done
    for i in ftpusers login.access; do
        edo mv "${IMAGE}"/usr/share/man/man5/{,k}${i}.5
    done
    for i in telnetd ftpd rshd popper; do
        edo mv "${IMAGE}"/usr/share/man/man8/{,k}${i}.8
        edo mv "${IMAGE}"/usr/${host}/libexec/{,k}${i}
    done

    # Do some removals to avoid several collisions with dev-libs/openssl manpages
    for i in DES_{cbc_cksum,cfb64_encrypt,ecb3_encrypt,ecb_encrypt,ede3_cbc_encrypt,is_weak_key} \
             DES_{key_sched,pcbc_encrypt,random_key,set_key,set_key_checked,set_key_unchecked} \
             DES_{set_odd_parity,string_to_key} \
             DH_{compute_key,free,generate_key,generate_parameters_ex,get_default_method,get_ex_data,new,new_method} \
             DH_{set_default_method,set_method,set_ex_data,size} \
             EVP_{aes_128_cbc,aes_192_cbc,aes_256_cbc} \
             EVP_{BytesToKey,CIPHER_CTX_block_size,CIPHER_CTX_cipher,CIPHER_CTX_cleanup,CIPHER_CTX_ctrl} \
             EVP_{CIPHER_CTX_flags,CIPHER_CTX_get_app_data,CIPHER_CTX_init,CIPHER_CTX_iv_length} \
             EVP_{CIPHER_CTX_key_length,CIPHER_CTX_mode,CIPHER_CTX_rand_key,CIPHER_CTX_set_app_data} \
             EVP_{CIPHER_CTX_set_key_length,CIPHER_block_size,CIPHER_iv_length,CIPHER_key_length} \
             EVP_{CipherFinal_ex,CipherInit_ex} \
             EVP_{CipherUpdate,DigestFinal_ex,DigestInit_ex,DigestUpdate,MD_CTX_block_size,MD_CTX_cleanup} \
             EVP_{MD_CTX_create,MD_CTX_destroy,MD_CTX_init,MD_CTX_md,MD_CTX_size,MD_block_size,MD_size} \
             EVP_{des_cbc,des_ede3_cbc,enc_null,get_cipherbyname,md2,md5,md_null,rc2_40_cbc,rc2_64_cbc} \
             EVP_{rc2_cbc,rc4,rc4_40,sha,sha1,sha256,sha384,sha512} OpenSSL_add_all_algorithms \
             PKCS5_PBKDF2_HMAC_SHA1 \
             RAND_{add,bytes,cleanup,file_name,get_rand_method,load_file,pseudo_bytes,seed,set_rand_method} \
             RAND_{status,write_file} RSA_{free,get_method,new,new_method,set_method}; do
        edo rm "${IMAGE}"/usr/share/man/man3/${i}.3
    done

    optionq afs && edo rm "${IMAGE}"/usr/${host}/bin/kpasswd
}

