# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge flag-o-matic systemd-service [ systemd_files=[ "racoon.service" ] ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Tools for configuring and using IPSEC"
DESCRIPTION="
Tools necessary for establishing keys for IPSEC connections including the rekeying during the
connection lifetime.
"
HOMEPAGE="http://ipsec-tools.sourceforge.net"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    admin [[ description = [ enable administration port for remote control (e.g. racoonctl) ] ]]
    idea [[ description = [ enable IDEA encryption ] ]]
    rc5 [[ description = [ enable RC5 encryption ] ]]
    selinux [[ description = [ enable SE-Linux based Kernel Security Context ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

AT_M4DIR=( . )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.8.2-CVE-2015-4047.patch
    "${FILES}"/${PN}-0.8.2-Don-t-link-against-libfl.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-dpd          # enable dead peer detection
    --enable-frag         # enable IKE fragmentation
    --enable-hybrid       # enable XAuth and Mode Configuration support
    --enable-ipv6
    --enable-natt         # enable NAT-Traversal
    --enable-shared
    --disable-gssapi      # TODO (abdulras) optionalise (GSSAPI/Kerberos) support (XAuth Server)
    --disable-static
    --with-kernel-headers=/usr/$(exhost --target)/include
    --with-readline
    --without-libldap     # TODO (abdulras) optionalise LDAP support (XAuth Server)
    --without-libpam      # TODO (abdulras) optionalise PAM authentication (XAuth Server)
    --without-libradius   # TODO (abdulras) optionalise RADIUS authentication (XAuth Server)
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'admin adminport'
    'admin stats'
    idea
    rc5
    'selinux security-context'
)

src_prepare() {
    # NOTE (abdulras) use of kernel headers in userspace is deprecated, but required for this package
    append-flags -Wno-cpp

    # remove -Werror
    edo sed \
        -e 's:-Werror::g' \
        -i configure.ac

    autotools_src_prepare
}

src_install() {
    default

    install_systemd_files

    keepdir /var/lib/racoon
}

