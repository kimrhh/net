# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ] systemd-service

SUMMARY="Message broker that implements the MQTT protocol versions 3.1 and 3.1.1"
DESCRIPTION="
Eclipse Mosquitto is an open source (EPL/EDL licensed) message broker that implements the MQTT
protocol versions 3.1 and 3.1.1. MQTT provides a lightweight method of carrying out messaging using
a publish/subscribe model. This makes it suitable for \"Internet of Things\" messaging such as with
low power sensors or mobile devices such as phones, embedded computers or microcontrollers like the
Arduino.
"
HOMEPAGE="https://mosquitto.org"
DOWNLOADS="${HOMEPAGE}/files/source/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/category/releases [[ lang = en ]]"

LICENCES="EDL-1.0 EPL-1.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    systemd
    tcpd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
        net-dns/c-ares
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        systemd? ( sys-apps/systemd )
        tcpd? ( sys-apps/tcp-wrappers )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.5-cmake.patch
    "${FILES}"/${PN}-1.5-libwrap.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_BINDIR:PATH=bin
    -DCMAKE_INSTALL_SYSCONFDIR:PATH=/etc/${PN}
    -DDOCUMENTATION:STRING=ON
    -DWITH_EC:STRING=ON
    -DWITH_PERSISTENCE:STRING=ON
    -DWITH_SOCKS:STRING=ON
    -DWITH_SRV:STRING=ON
    -DWITH_STATIC_LIBRARIES:STRING=OFF
    -DWITH_SYS_TREE:STRING=ON
    -DWITH_THREADING:STRING=ON
    -DWITH_TLS:STRING=ON
    -DWITH_TLS_PSK:STRING=ON
    -DWITH_WEBSOCKETS:STRING=OFF
)

src_prepare() {
    cmake_src_prepare

    edo sed \
        -e "s:^#autosave_interval:autosave_interval:" \
        -e "s:^#persistence false$:persistence true:" \
        -e "s:^#persistence_file:persistence_file:" \
        -e "s:^#persistence_location$:persistence_location /var/lib/mosquitto/:" \
        -i mosquitto.conf
}

src_configure() {
    local cmakeparams=(
        -DUSE_LIBWRAP:STRING=$(option tcpd ON OFF)
        -DWITH_SYSTEMD:STRING=$(option systemd ON OFF)
    )

    ecmake \
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        "${cmakeparams[@]}"
}

src_install() {
    cmake_src_install

    keepdir /var/lib/${PN}
    edo chown -R mosquitto:mosquitto "${IMAGE}"/var/lib/${PN}

    install_systemd_files
}

