# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Richard Brown
# Copyright 2010 Ali Polatel <alip@exherbo.org>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python freedesktop-desktop

export_exlib_phases src_prepare src_install

SUMMARY="A utility for scanning networks"
DESCRIPTION="
nmap (Network Mapper) is a utility for network exploration, administration, and
security auditing. It uses IP packets in novel ways to determine which hosts are
available online (host discovery), which TCP/UDP ports are open (port scanning),
and what applications and services are listening on each port (version detection).
It can also identify remote host OS and device types via TCP/IP fingerprinting.
Nmap offers flexible target and port specifications, decoy/stealth scanning for
firewall and IDS evasion, and highly optimized timing algorithms for fast scanning.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="${HOMEPAGE}/dist/${PNV}.tar.bz2"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"

UPSTREAM_CHANGELOG="${HOMEPAGE}/changelog.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs.html"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    gtk   [[ description = [ Build Zenmap, the GTK+ GUI ] requires = utils ]]
    lua   [[ description = [ Enable Lua scripting support (required for NSE) ] ]]
    utils [[ description = [ Include additional Ndiff, Nping, and Ncat utilities ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# NOTE(somasis): all tests depend way too much on having unsandboxed network caps!
RESTRICT="test"

# FIXME: bundled libdnet (recommended upstream)
# FIXME: bundled liblinear
DEPENDENCIES="
    build+run:
        dev-libs/libpcap[>=1.7.3]
        dev-libs/pcre[>=7.6]
        net-libs/libssh2[>=1.8.0]
        sys-libs/zlib[>=1.2.8]
        gtk? ( dev-lang/python:=[>=2.4&<3][sqlite] )
        lua? ( dev-lang/lua:=[>=5.3&<5.4] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        utils? ( dev-lang/python:*[>=2.4&<3] ) [[ note = [ Python required for Ndiff ] ]]
    run:
        gtk? ( gnome-bindings/pygtk:2 )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    PYTHON="python2"
    --enable-nls
    --with-libpcap
    --with-libpcre
    --with-libssh2
    --with-libz
    --with-openssl
    --without-nmap-update
    --without-subversion
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "gtk zenmap"
    "lua liblua"
    "utils ndiff"
    "utils nping"
    "utils ncat"
)

DEFAULT_SRC_COMPILE_PARAMS=( AR="${AR}" RANLIB="${RANLIB}" PYTHON="python2" )

DEFAULT_SRC_INSTALL_PARAMS=( STRIP=: )

nmap_src_prepare() {
    # Don't strip binaries
    edo sed -e "/^\t\$(INSTALL)/s: -s::g" -i ncat/Makefile.in

    # Make sure we install python libs in exec_prefix
    # Don't byte compile python libs on install
    edo sed \
        -e "/cd \$(ZENMAPDIR) && \$(PYTHON) setup.py --quiet install/s:$: --no-compile --prefix /usr --install-lib $(python_get_sitedir) --install-scripts /usr/$(exhost --target)/bin:" \
        -e "/cd \$(NDIFFDIR) && \$(PYTHON) setup.py install/s:$: --no-compile --prefix /usr --install-lib $(python_get_sitedir) --install-scripts /usr/$(exhost --target)/bin:" \
        -i Makefile.in

    default
}

nmap_src_install() {
    default

    option gtk && python_bytecompile
}

